name := """social1"""
organization := "com.victor"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  guice,
"commons-codec" % "commons-codec" % "1.11"
)


