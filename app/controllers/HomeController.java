package controllers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;


public class HomeController extends Controller {

  //TODO: These harcoded strings should be re-factored to be picked from configuation
  public static final String X_SHOPIFY_HMAC_SHA256 = "X-Shopify-Hmac-Sha256";
  private final String SECRET = "5996e880c0d03332940b080c13834c04608f2434ce6a435e63333e2ac400ca50";


  //Entry point of controller
  public Result index()
      throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {

    //Not required because request() can be used straightaway
    //Nevertheless using it to bring it as close to ShopSocial code
    Http.Context ctx = Http.WrappedContext.current();

    //TODO: Use ctx.request.getHeaders() instead since getHeader() is deprecated
    String SignatureFromHeader = ctx.request().getHeader(X_SHOPIFY_HMAC_SHA256);
    String shopifyPayload = ctx.request().body().asJson().toString();

    Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
    SecretKeySpec secret_key = new SecretKeySpec(SECRET.getBytes("UTF-8"), "HmacSHA256");
    sha256_HMAC.init(secret_key);

    String encoded = Base64.getEncoder()
        .encodeToString(sha256_HMAC.doFinal(shopifyPayload.getBytes("UTF-8")));
    if (encoded.equals(SignatureFromHeader)) {
      return ok("TRUE");
    } else {
      return ok("FALSE");
    }

  }

}
